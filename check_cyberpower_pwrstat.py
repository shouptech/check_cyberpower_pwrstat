#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script to check the status of a CyberPower UPS.

For use with Nagios.
"""

import socket
import sys
import argparse

def critical_message(message):
    "Formats a critical message"
    return ('CRITICAL - ' + str(message), 2)

def warning_message(message):
    "Formats a warning message"
    return ('WARNING - ' + str(message), 1)

def success_message(message):
    "Formats a success message"
    return ('OK - ' + str(message), 0)

def unknown_message(message):
    "Formats an unknown message"
    return ('UNKNOWN - ' + str(message), 3)

def read_pwrstat(socket_path="/var/pwrstatd.ipc"):
    """
    Connects to pwrstatd's socket and returns the status data as a string.
    """
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(socket_path)
    sock.sendall("STATUS\n\n")
    data = sock.recv(2048)
    sock.close()

    stats = {}
    for line in data.split("\n"):
        columns = line.split("=")
        if len(columns) > 1:
            stats[columns[0]] = columns[1]

    return stats

def check_runtime(stats, warning=300, critical=60):
    "Check the runtime remaining"
    runtime = int(stats['battery_remainingtime'])
    message = 'Runtime remaing: ' + str(runtime) + ' seconds'

    if runtime < int(critical):
        return critical_message(message)
    if runtime < int(warning):
        return warning_message(message)
    return success_message(message)

def check_load(stats, warning=70, critical=90):
    "Check the load"
    load_percent = float(stats['load'])/1000
    capacity = float(stats['output_rating_watt'])/1000
    load_watt = load_percent*capacity/100

    message = 'Load: ' + str(load_percent) + '% (' + str(load_watt) + ' Watt)'

    if load_percent > float(critical):
        return critical_message(message)
    if load_percent > float(warning):
        return warning_message(message)
    return success_message(message)

def check_cacti(stats, warning=None, critical=None):
    "Creates output for use with Cacti"
    runtime = int(stats['battery_remainingtime'])
    load_percent = float(stats['load'])/1000
    capacity = float(stats['output_rating_watt'])/1000
    load_watt = load_percent*capacity/100
    model = stats['model_name']

    message = 'model:' + str(model) + ' runtime:' + str(runtime) + \
        ' capacity:' + str(capacity) + ' load_watt:' + str(load_watt) + \
        ' load_percent:' + str(load_percent)

    return (message, 0)

def main():
    "Perform checks on CyberPower UPS"
    checks = {'runtime': check_runtime, 'load': check_load, 'cacti': check_cacti}

    parser = argparse.ArgumentParser(description="Check status of UPS unit")
    parser.add_argument(
        '-w', '--warning',
        help="Threshold for warning. Varies depending on check performed.")
    parser.add_argument(
        '-c', '--critical',
        help="Threshold for critical. Varies depending on check performed.")
    parser.add_argument(
        '-s', '--socket', default="/var/pwrstatd.ipc",
        help="Socket for pwrstatd. Default should suffice on most systems.")
    parser.add_argument(
        'check', help="Check to perform.", choices=list(checks.keys()))
    args = parser.parse_args()

    stats = read_pwrstat(args.socket)
    if args.warning and args.critical:
        (message, state_code) = checks[args.check](
            stats, args.warning, args.critical)
    elif args.warning:
        (message, state_code) = checks[args.check](stats, args.warning)
    elif args.critical:
        (message, state_code) = checks[args.check](
            stats, critical=args.critical)
    else:
        (message, state_code) = checks[args.check](stats)

    print message
    return state_code

if __name__ == "__main__":
    sys.exit(main())
